name = input("Введите Ваше имя: ")
surname = input("Введите Вашу фамилию: ")
age = float(input("Введите Ваш возраст: "))
gender = input("Введите Ваш пол(м/ж): ")
if age >= 61.5 and gender == 'м':
    print(f"Уважаемый {surname} {name}, Вам начислена единоразовая выплата в размере 10000 руб.")
elif age >= 56.5 and gender == 'ж':
    print(f"Уважаемая {surname} {name}, Вам начислена единоразовая выплата в размере 10000 руб.")
else:
    print(f"{surname} {name}, Вам не начислена единоразовая выплата, так как Вы не являетесь пенсионером.")
